#include "alienHit.h"

#include "general.h"

AlienHit::AlienHit(const b2Vec2& pos, AlienType alienType)
{
	mEventType = Event::ALIEN_HIT;
	mActedUpon = false;
	
	mPos = pos;
	mAlienType = alienType;
}

AlienHit::~AlienHit()
{

}
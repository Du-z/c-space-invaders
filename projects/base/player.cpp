#include "player.h"

#include "eventMgr.h"
#include "keyPressed.h"
#include "playerFired.h"
#include "playerHit.h"
#include "collision.h"
#include "bullet.h"

#include "general.h"
#include "resourceMgr.h"

int Player::mPlayerLives = 3;

Player::Player()
{
	mActorType = Actor::PLAYER;

	moveType = MoveType::NONE;

	fireCountdown = 0;
	roundPerSecond = 120;
}

Player::~Player()
{

}

 void Player::setup(b2World* pB2World)
{
	/////////////// SETUP Box 2D /////////////////
	b2BodyDef bodyDef;
	bodyDef.position.Set(screenWidthM / 2, screenHeightM - 1.f);
	bodyDef.type = b2_kinematicBody;
	bodyDef.fixedRotation = true;
	pBody = pB2World->CreateBody(&bodyDef);

	b2Vec2 size(2, 1); // Set size here
	b2PolygonShape shape;
	shape.SetAsBox(size.x / 2, size.y / 2, b2Vec2(0, 0), 0.f);

	b2FixtureDef fixtureDef;
    fixtureDef.friction = 0.7f; // set the friction the obj provide when it rubs on another obj
	fixtureDef.restitution = 0.25f; // set the elasticity of the obj
    fixtureDef.shape = &shape;

	pBody->CreateFixture(&fixtureDef);
	pBody->SetUserData(this);
	//////////////////////////////////////////////

	/////////////// SETUP DRAWABLE ///////////////
	pDrawable = new Drawable();
	pDrawable->setup("data/sprites.png");
	pDrawable->setTextureRect(0, 23, 18, 9);
	pDrawable->setSize(size);
	pDrawable->setOrigin(sf::Vector2f(18.f / 2, 9.f / 2));
	pDrawable->setColour(sf::Color::Green);
	//////////////////////////////////////////////

	/////////////// SETUP Sound /////////////////
	shoot = new sf::Sound;
	shoot->setBuffer(*ResourceMgr::GetInstance()->getSound("data/sound/playerShoot.wav"));

	playerHit = new sf::Sound;
	playerHit->setBuffer(*ResourceMgr::GetInstance()->getSound("data/sound/playerHit.wav"));
	//////////////////////////////////////////////

	/////////////// SUB TO EVENTS ////////////////
	EventMgr::GetInstance()->addSubscriber(Event::KEY_PRESSED, this);
	//////////////////////////////////////////////

	mPlayerLives = 3;
}

void Player::simulation(sf::RenderWindow& window, float deltaT)
{
	pDrawable->setPosRot(*pBody);

	b2Vec2 newPos= pBody->GetPosition();
	if(moveType == LEFT && newPos.x >= 1.5f)
	{
		newPos.x += -15 * deltaT;
	}
	else if(moveType == RIGHT && newPos.x <= screenWidthM - 1.5f)
	{
		newPos.x += 15 * deltaT;
	}

	if(newPos.x != 0)
	{
		pBody->SetTransform(newPos, 0);
	}
	moveType = NONE;

	fireCountdown -= deltaT;
}

void Player::eventReceiver(ABEvent* theEvent)
{
	// Check the event type
	if(theEvent->getEventType() == Event::KEY_PRESSED)
	{
		KeyPressed* key = static_cast<KeyPressed*>(theEvent);
		
		// LEFT/RIGHT
		if(key->getKey() == sf::Keyboard::Right)
		{
			moveType = RIGHT;
		}
		else if(key->getKey() == sf::Keyboard::Left)
		{
			moveType = LEFT;
		}
		else if(key->getKey() == sf::Keyboard::Space)
		{
			if(fireCountdown <= 0)
			{
				b2Vec2 pos(pBody->GetPosition().x, pBody->GetPosition().y - 0.75f);
			
				EventMgr::GetInstance()->addEvent(new PlayerFired(pos));
				fireCountdown = 60 / roundPerSecond;

				shoot->play();
			}
		}
	}
	else if(theEvent->getEventType() == Event::COLLISION)
	{
		Collision* collision = static_cast<Collision*>(theEvent);
		if(collision->getOtherBody()->getActorType() == Actor::BULLET)
		{
			Bullet* bullet = static_cast<Bullet*>(collision->getOtherBody());
			if(bullet->getShotFrom() == Actor::ALIEN)
			{
				Player::mPlayerLives--;
				playerHit->play();
				EventMgr::GetInstance()->addEvent(new PlayerHit(pBody->GetPosition()));
			}
		}
	}
}

void Player::render(sf::RenderWindow& window)
{
	pDrawable->draw(window);
}

void Player::dispose()
{
	pDrawable->dispose();
	delete pDrawable;
	pDrawable = NULL;

	pBody->GetWorld()->DestroyBody(pBody);
	pBody = NULL;
}

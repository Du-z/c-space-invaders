#ifndef H_DRAWABLE
#define H_DRAWABLE

#include "SFML/Graphics.hpp"
#include "Box2D/Box2D.h"
#include "qgf2d/anim.h"

#include "general.h"

// any time a Box 2D vector is used it is presumed that it is in meter units, not pixels (16px = 1m)

class Drawable
{
public:
	Drawable();
	~Drawable();
 
	// sets up the texture/sprite.
	void setup(const std::string& textureDir);
	void setup(const std::string& textureDir,const b2Vec2& size);

	void draw(sf::RenderWindow& window){if(mSprite != NULL){window.draw(*mSprite);}}

	void setPos(const sf::Vector2f& pos){setPosRot(pos, mSprite->getRotation());}
	void setPosRot(const sf::Vector2f& pos, float rot);
	void setPosRot(const b2Body& body){setPosRot(sf::Vector2f(body.GetPosition().x * worldScale, body.GetPosition().y * worldScale), body.GetAngle() * 180 / b2_pi);}

	void setSize(const b2Vec2& size);
	void setSize(const sf::Vector2f& size);
	void setSize(float x, float y);
	void setScale(float scale){mSprite->setScale(scale, scale);}
	void setTextureRect(int left, int top, int width, int height);

	void setOrigin(const sf::Vector2f& orig){mSprite->setOrigin(orig);}

	void setFlipX();
	bool getIsFliped(){return isFliped;}

	void addAnim(int startX, int startY, int frameWidth, int frameHeight, int columns, int rows); // if mAnim != NULL delete it and creates a new one
	void addAnimSequence(const std::string &name, int frameStart, int frameEnd, float length, bool looping){mAnim->addSequence(name, frameStart, frameEnd, length, looping);}
	void animUpdate(float deltaT){mAnim->update(deltaT);}
	void animPlay(const std::string &name, bool restart){mAnim->play(name, restart);}

	void setColour(const sf::Color& colour){mSprite->setColor(colour);}
	sf::Color getColour(){return mSprite->getColor();}

	sf::Sprite* getSprite(){return mSprite;}

	void dispose();

private:
	sf::Texture* mTexture;
	sf::Sprite* mSprite;
	qgf::Anim* mAnim;

	bool isFliped;
};

#endif



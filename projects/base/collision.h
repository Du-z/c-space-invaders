#ifndef H_COLLISION
#define H_COLLISION

#include "event.h"
#include "actor.h"

class Collision : public ABEvent
{
public:
	explicit Collision(ABActor* otherBody);
	virtual ~Collision();

	ABActor* getOtherBody(){return mOtherBody;}

private:

	ABActor* mOtherBody;
};

#endif // H_COLLISION
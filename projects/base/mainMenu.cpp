#include "mainMenu.h"

#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "kf/kf_vector2.h"

#include "keyboard.h"
#include "background.h"
#include "MenuLabel.h"

#include "resourceMgr.h"

ScreenTypes MainMenu::nextScreenSelected = NO_CHANGE;

MainMenu::MainMenu()
{

}

MainMenu::~MainMenu()
{

}

void MainMenu::setup(sf::RenderWindow& window)
{
	
	//*** ADD GAME OBJECTS **//

	ABActor* keyboard = new Keyboard();
	keyboard->setup(NULL);
	actorList.push_back(keyboard);

	ABActor* bg = new Background();
	bg->setup(NULL);
	actorList.push_back(bg);

	ABActor* lbl = new MenuLabel();
	lbl->setup(NULL);
	actorList.push_back(lbl);

	sf::Font* font = ResourceMgr::GetInstance()->getFont("data/victor-pixel.ttf");

	titleText.setFont(*font);
	titleText.setCharacterSize(80);
	titleText.setString("Space Invaders!");
	titleText.setColor(sf::Color::Red);
	titleText.setPosition(screenWidth / 2, 40);
	titleText.setOrigin(titleText.getLocalBounds().width / 2, titleText.getLocalBounds().height / 2);

	font = ResourceMgr::GetInstance()->getFont("data/arial.ttf");
	instructText.setFont(*font);
	instructText.setCharacterSize(18);
	instructText.setString("Use arrow keys to make selection,\nSpacebar to confirm.");
	instructText.setPosition(screenWidth / 2, screenHeight / 2 + 50);
	instructText.setOrigin(instructText.getLocalBounds().width / 2, instructText.getLocalBounds().height / 2);

	nextScreenSelected = NO_CHANGE;
}

void MainMenu::simulation(sf::RenderWindow& window, float deltaT)
{
	
	//simulate the vector of actors

	sf::Event ev;
	while (window.pollEvent(ev))
	{
		if (ev.type == sf::Event::Closed)
		{
			nextScreen = EXIT;
		}
	}

	// draw the vector of actors
	for(size_t i = 0, size = actorList.size(); i < size; ++i)
	{
		actorList[i]->simulation(window, deltaT);
	}

	if(nextScreenSelected != NO_CHANGE)
		nextScreen = nextScreenSelected;

}

void MainMenu::render(sf::RenderWindow& window)
{
	window.clear();

	// draw the vector of actors
	for(size_t i = 0, size = actorList.size(); i < size; ++i)
	{
		actorList[i]->render(window);
	}

	window.draw(titleText);
	window.draw(instructText);

	window.display();
}

void MainMenu::dispose()
{
	for(size_t i = 0, size = actorList.size(); i < size; ++i)
	{
		actorList[i]->dispose();
		delete actorList[i];
		actorList[i] = NULL;
	}
}
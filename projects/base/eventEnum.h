#ifndef H_EVENT_ENUM
#define H_EVENT_ENUM

namespace Event
{
	enum Types 
	{
		UNSET = -1,
		KEY_PRESSED,
		PLAYER_FIRED,
		ALIEN_FIRED,
		COLLISION,
		ALIEN_HIT,
		PLAYER_HIT,
		COUNT
	};
}

#endif // H_EVENT_ENUM
#include "alien.h"

#include "resourceMgr.h"

#include "eventMgr.h"
#include "alienFired.h"
#include "alienHit.h"

#include "general.h"
#include "collision.h"
#include "bullet.h"
#include "gameLoop.h"

int Alien::mAliensAlive = 0;

MoveDirection Alien::mMoveDirection = M_RIGHT;
MoveDirection Alien::mNextFrameMoveDirection = M_RIGHT;

float Alien::nextMinHeight = -1;
float Alien::rowHeight = 0;

float Alien::mshipTimer = 10;

Alien::Alien(AlienType alienType)
{
	mActorType = Actor::ALIEN;

	mAlienType = alienType;

	kill = false;
}

Alien::~Alien()
{

}

void Alien::setup(b2World* pB2World)
{

	// figure out the size
	b2Vec2 size(1, 1); // Set size here
	if(mAlienType == TIER_1)
		size.Set(1.5f, 1.5f);
	else if(mAlienType == TIER_2)
		size.Set(1.875f, 1.5f);
	else if(mAlienType == TIER_3)
		size.Set(2.25f, 1.5f);
	else if(mAlienType == MOTHERSHIP)
		size.Set(4.f, 1.5f);

	// figure out the pos
	float posX = -64, posY = -64;
	if(mAlienType != MOTHERSHIP)
	{
		const int numCols = 10, numRows = 6, offsetX = 6, offsetTopY = 4, offsetBottomY = 20;

		rowHeight = (screenHeightM - offsetTopY - offsetBottomY) / numRows;

		posX = offsetX + ((screenWidthM - offsetX - offsetX) / numCols) * (mAliensAlive % numCols);
		posY = offsetTopY + rowHeight * (mAliensAlive / numCols); // yay int math to the rescue

		mAliensAlive++;
	}
	else
	{
		mshipTimer = 10;
	}

	startPos.Set(posX, posY);

	/////////////// SETUP Box 2D /////////////////
	b2BodyDef bodyDef;
	bodyDef.position.Set(posX, posY);
	bodyDef.type = b2_kinematicBody;
	bodyDef.fixedRotation = true;
	pBody = pB2World->CreateBody(&bodyDef);

	b2PolygonShape shape;
	shape.SetAsBox(size.x / 2, size.y / 2, b2Vec2(0, 0), 0.f);

	b2FixtureDef fixtureDef;
	fixtureDef.friction = 0.7f; // set the friction the obj provide when it rubs on another obj
	fixtureDef.restitution = 0.25f; // set the elasticity of the obj
	fixtureDef.isSensor = true;
	fixtureDef.shape = &shape;

	pBody->CreateFixture(&fixtureDef);
	pBody->SetUserData(this);

	//////////////////////////////////////////////

	/////////////// SETUP DRAWABLE ///////////////
	pDrawable = new Drawable();
	pDrawable->setup("data/sprites.png");

	if(mAlienType == TIER_1)
	{
		pDrawable->addAnim(120, 0, 16, 16, 1, 2);
		pDrawable->setOrigin(sf::Vector2f(16.f / 2, 16.f / 2));
	}
	else if(mAlienType == TIER_2)
	{
		pDrawable->addAnim(98, 0, 22, 16, 1, 2);
		pDrawable->setOrigin(sf::Vector2f(22.f / 2, 16.f / 2));
	}
	else if(mAlienType == TIER_3)
	{
		pDrawable->addAnim(74, 0, 24, 16, 1, 2);
		pDrawable->setOrigin(sf::Vector2f(24.f / 2, 16.f / 2));
	}
	else if(mAlienType == MOTHERSHIP)
	{
		pDrawable->addAnim(0, 1, 48, 21, 1, 1);
		pDrawable->setOrigin(sf::Vector2f(48.f / 2, 21.f / 2));
		pDrawable->setColour(sf::Color::Red);
	}

	pDrawable->addAnimSequence("move", 0, 1, 0.8f, true);
	pDrawable->animPlay("move", false);

	pDrawable->setSize(size);
	//////////////////////////////////////////////

	/////////////// SETUP Sound /////////////////
	if(mAlienType == MOTHERSHIP)
	{
		mShipFly = new sf::Sound;
		mShipFly->setBuffer(*ResourceMgr::GetInstance()->getSound("data/sound/mShipFly.wav"));

		alienHit = new sf::Sound;
		alienHit->setBuffer(*ResourceMgr::GetInstance()->getSound("data/sound/mShipHit.wav"));
	}
	else
	{
		mShipFly = NULL;
		
		alienHit = new sf::Sound;
		alienHit->setBuffer(*ResourceMgr::GetInstance()->getSound("data/sound/alienHit.wav"));
	}
	//////////////////////////////////////////////

	/////////////// SUB TO EVENTS ////////////////

	//////////////////////////////////////////////
}

void Alien::reset()
{
	pBody->SetTransform(startPos, 0);
	pBody->SetActive(true);

	pDrawable->animPlay("move", true);

	// reset most off the static vars (not very efficient to do it for each alien, but its no biggie in this case.)
	mAliensAlive = tier1AlienCount + tier2AlienCount + tier3AlienCount;
	mMoveDirection = M_RIGHT;
	mNextFrameMoveDirection = M_RIGHT;
	nextMinHeight = -1;
	mshipTimer = 10;
}

void Alien::simulation(sf::RenderWindow& window, float deltaT)
{
	if(pBody->IsActive())
	{
		if(mAlienType == MOTHERSHIP)
		{
			if(pBody->GetPosition().x < 0 && pBody->GetLinearVelocity().x <= 0 || pBody->GetPosition().x > screenWidthM && pBody->GetLinearVelocity().x >= 0)
			{
				pBody->SetTransform(b2Vec2(-64.f, -64.f), 0);
				pBody->SetActive(false);
			}
		}
		else
		{
			alienControls();
			alienShoot();
		}

		if(kill)
		{
			EventMgr::GetInstance()->addEvent(new AlienHit(pBody->GetPosition(), mAlienType));

			pBody->SetTransform(b2Vec2(-64.f, -64.f), 0);
			pBody->SetActive(false);

			if(mAlienType != MOTHERSHIP)
				Alien::mAliensAlive--;
			kill = false;
		}

		pDrawable->setPosRot(*pBody);
		pDrawable->animUpdate(deltaT);
	}
	else
	{
		if(mAlienType == MOTHERSHIP)
		{
			mshipTimer -= deltaT;

			if(mshipTimer <= 0)
			{
				pBody->SetActive(true);
				pBody->SetTransform(b2Vec2(-2, 3), 0);
				pBody->SetLinearVelocity(b2Vec2(6, 0));

				mShipFly->play();

				mshipTimer = 10;
			}
		}
	}
}

void Alien::alienControls()
{
	float multi =  (GameLoop::getLevel() - 1) / 2; // speed scales as the levels increase
	float minSpd = 3 + multi, maxSpd = 15 + multi;
	float speed = (maxSpd - minSpd) / mAliensAlive + minSpd;
	
	if(mMoveDirection == M_RIGHT)
	{
		if(pBody->GetLinearVelocity().x != speed)
		{
			pBody->SetLinearVelocity(b2Vec2(speed,0));
		}
		else
		{
			if(pBody->GetPosition().x > screenWidthM - 2)
				mNextFrameMoveDirection = M_LEFT;
		}
	}
	else if(mMoveDirection == M_LEFT)
	{
		if(pBody->GetLinearVelocity().x != -speed)
		{
			pBody->SetLinearVelocity(b2Vec2(-speed,0));
		}
		else
		{
			if(pBody->GetPosition().x < 2)
				mNextFrameMoveDirection = M_RIGHT;
		}
	}
	else if(mMoveDirection == M_DOWN)
	{
		if(pBody->GetLinearVelocity().y != speed)
		{
			pBody->SetLinearVelocity(b2Vec2(0, speed));

			float minHeightCheck = pBody->GetPosition().y + rowHeight / 2;
			if(minHeightCheck > nextMinHeight)
			{
				nextMinHeight = minHeightCheck;
			}
		}
		else
		{
			if(pBody->GetPosition().y >= nextMinHeight)
			{
				mMoveDirection = mNextFrameMoveDirection;
				nextMinHeight = -1;
			}
		}
	}
}

void Alien::alienShoot()
{
	if(randInt(0,8000) == 0)
	{
		EventMgr::GetInstance()->addEvent(new AlienFired(pBody->GetPosition()));
	}
}

void Alien::eventReceiver(ABEvent* theEvent)
{
	if(theEvent->getEventType() == Event::COLLISION)
	{
		Collision* collision = static_cast<Collision*>(theEvent);
		if(collision->getOtherBody()->getActorType() == Actor::BULLET)
		{
			Bullet* bullet = static_cast<Bullet*>(collision->getOtherBody());
			if(bullet->getShotFrom() == Actor::PLAYER)
			{
				kill = true;
				if(mShipFly != NULL)
					mShipFly->stop();
				alienHit->play();
			}
		}
	}
}

void Alien::render(sf::RenderWindow& window)
{
	if(pBody->IsActive())
		pDrawable->draw(window);
}

void Alien::dispose()
{
	pDrawable->dispose();
	delete pDrawable;
	pDrawable = NULL;

	pBody->GetWorld()->DestroyBody(pBody);
	pBody = NULL;
}

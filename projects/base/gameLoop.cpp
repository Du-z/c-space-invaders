#include "gameLoop.h"

#include <sstream>
#include "iostream"
#include "kf/kf_log.h"
#include "SFML/Window.hpp"

#include "resourceMgr.h"
#include "debugDraw.h"
#include "general.h"

#include "keyboard.h"
#include "background.h"
#include "player.h"
#include "bullet.h"
#include "alien.h"
#include "explosion.h"
#include "debris.h"
#include "shield.h"
#include "gui.h"

int GameLoop::level = 1;

GameLoop::GameLoop()
{
	collisionMgr = NULL;
}

GameLoop::~GameLoop()
{

}

void GameLoop::setup(sf::RenderWindow& window)
{
	
	Alien::mAliensAlive = 0;

	Alien::mMoveDirection = M_RIGHT;
	Alien::mNextFrameMoveDirection = M_RIGHT;

	Alien::nextMinHeight = -1;

	//setup physics world
	b2Vec2 gravity(0, 9.8f); //normal earth gravity, 9.8 m/s/s straight down!

	pB2World = new b2World(gravity);
	pB2World->SetAllowSleeping(true);

	collisionMgr = new CollisionMgr();
	pB2World->SetContactListener(collisionMgr);

	// I need to make a preprocessor for this so it wont run in release
	DebugDraw* debugdraw = new DebugDraw(); 
	debugdraw->LinkTarget(window);
	pB2World->SetDebugDraw(debugdraw); // Add the debugDraw to the B2D World
	
	//*** ADD GAME OBJECTS **//

	ABActor* keyboard = new Keyboard();
	keyboard->setup(pB2World);
	actorList.push_back(keyboard);

	ABActor* bg = new Background();
	bg->setup(pB2World);
	actorList.push_back(bg);

	ABActor* gui = new Gui();
	gui->setup(pB2World);
	actorList.push_back(gui);

	for(int i = 0; i < bulletCount; ++i)
	{
		ABActor* bullet = new Bullet();
		bullet->setup(pB2World);
		actorList.push_back(bullet);
	}

	for(int i = 0; i < tier1AlienCount; ++i)
	{
		ABActor* alien = new Alien(TIER_1);
		alien->setup(pB2World);
		actorList.push_back(alien);
	}

	for(int i = 0; i < tier2AlienCount; ++i)
	{
		ABActor* alien = new Alien(TIER_2);
		alien->setup(pB2World);
		actorList.push_back(alien);
	}

	for(int i = 0; i < tier3AlienCount; ++i)
	{
		ABActor* alien = new Alien(TIER_3);
		alien->setup(pB2World);
		actorList.push_back(alien);
	}

	for(int i = 0; i < mshipAlienCount; ++i)
	{
		ABActor* alien = new Alien(MOTHERSHIP);
		alien->setup(pB2World);
		actorList.push_back(alien);
	}

	for(int i = 0; i < explosionCount; ++i)
	{
		ABActor* explosion = new Explosion();
		explosion->setup(pB2World);
		actorList.push_back(explosion);
	}

	for(int i = 0; i < debrisCount; ++i)
	{
		ABActor* debris = new Debris();
		debris->setup(pB2World);
		actorList.push_back(debris);
	}

	for(int i = 0; i < shieldCount; ++i)
	{
		ABActor* shield = new Shield();
		shield->setup(pB2World);
		actorList.push_back(shield);
	}

	ABActor* player = new Player();
	player->setup(pB2World);
	actorList.push_back(player);

	// Do some after load debug stuff
	std::stringstream ss;
	float texSize = (float) ResourceMgr::GetInstance()->getTextureSize() / 1024 / 1024;
	float totSize = (float) ResourceMgr::GetInstance()->getAssetsSize() / 1024 / 1024;

	ss.unsetf(std::ios::floatfield);
	ss.precision(3);
	ss << "GameLoop loaded...\n\t Texture Mem = " << texSize << "MB\n\t Total Mem = " << totSize << " MB";
	kf_log(ss.str());
	// end after load debug stuff
}

void GameLoop::simulation(sf::RenderWindow& window, float deltaT)
{
	sf::Event ev;
	while (window.pollEvent(ev))
	{
		if ((ev.type == sf::Event::Closed))
			nextScreen = EXIT;
		else if(ev.type == sf::Event::KeyPressed && ev.key.code == sf::Keyboard::Escape)
			nextScreen = MAIN_MENU;
	}

	if(Alien::mMoveDirection != M_DOWN && Alien::mMoveDirection != Alien::mNextFrameMoveDirection)
		Alien::mMoveDirection = M_DOWN;

	for(size_t i = 0, size = actorList.size(); i < size; ++i)
	{
		actorList[i]->simulation(window, deltaT);
	}

	pB2World->Step(1/60.0, 8, 3);

	if(Alien::mAliensAlive <= 0) // next level check
	{
		nextLevel();
	}
	else if(Alien::nextMinHeight >= screenHeightM - 3.8f || Player::getPlayerLives() <= 0)
	{
		endGame();
	}
}

void GameLoop::nextLevel()
{
	level++;

	for(size_t i = 0, size = actorList.size(); i < size; ++i)
	{
		if(actorList[i]->getActorType() == Actor::ALIEN)
		{
			Alien* alien = static_cast<Alien*>(actorList[i]);

			alien->reset();
		}
	}
}

void GameLoop::endGame()
{
	level = 0;

	nextScreen = MAIN_MENU;
}

void GameLoop::render(sf::RenderWindow& window)
{
	window.clear();

	for(size_t i = 0, size = actorList.size(); i < size; ++i)
	{
		actorList[i]->render(window);
	}

	//Draw the debug box2D data
	pB2World->DrawDebugData();

	window.display();
}

void GameLoop::dispose()
{
	for(size_t i = 0, size = actorList.size(); i < size; ++i)
	{
		actorList[i]->dispose();
		delete actorList[i];
		actorList[i] = NULL;
	}

	delete pB2World;
	pB2World = NULL;

	delete collisionMgr;
	collisionMgr = NULL;
}
#include "credits.h"

#include "SFML/Graphics.hpp"
#include "SFML/Window.hpp"
#include "kf/kf_vector2.h"

#include "keyboard.h"
#include "background.h"
#include "MenuLabel.h"

#include "resourceMgr.h"

Credits::Credits()
{

}

Credits::~Credits()
{

}

void Credits::setup(sf::RenderWindow& window)
{
	
	//*** ADD GAME OBJECTS **//

	ABActor* keyboard = new Keyboard();
	keyboard->setup(NULL);
	actorList.push_back(keyboard);

	ABActor* bg = new Background();
	bg->setup(NULL);
	actorList.push_back(bg);

	sf::Font* font = ResourceMgr::GetInstance()->getFont("data/victor-pixel.ttf");
	titleText.setFont(*font);
	titleText.setCharacterSize(80);
	titleText.setString("Credits!");
	titleText.setColor(sf::Color::Red);
	titleText.setPosition(screenWidth / 2, 40);
	titleText.setOrigin(titleText.getLocalBounds().width / 2, titleText.getLocalBounds().height / 2);

	creditText.setFont(*font);
	creditText.setCharacterSize(40);
	creditText.setString("Programming:\n\tBrian Duhs\nArtwork:\n\tgoo.gl/NNdTl\n\n\t\tPress enter to continue.");
	creditText.setPosition(screenWidth / 2, screenHeight / 2);
	creditText.setOrigin(creditText.getLocalBounds().width / 2, creditText.getLocalBounds().height / 2);
}

void Credits::simulation(sf::RenderWindow& window, float deltaT)
{
	
	//simulate the vector of actors

	sf::Event ev;
	while (window.pollEvent(ev))
	{
		if (ev.type == sf::Event::Closed)
		{
			nextScreen = EXIT;
		}
		else if(ev.type == sf::Event::KeyPressed)
		{
			if(ev.key.code == sf::Keyboard::Space)
				nextScreen = MAIN_MENU;
		}
	}

	// draw the vector of actors
	for(size_t i = 0, size = actorList.size(); i < size; ++i)
	{
		actorList[i]->simulation(window, deltaT);
	}


}

void Credits::render(sf::RenderWindow& window)
{
	window.clear();

	// draw the vector of actors
	for(size_t i = 0, size = actorList.size(); i < size; ++i)
	{
		actorList[i]->render(window);
	}

	window.draw(titleText);
	window.draw(creditText);

	window.display();
}

void Credits::dispose()
{
	for(size_t i = 0, size = actorList.size(); i < size; ++i)
	{
		actorList[i]->dispose();
		delete actorList[i];
		actorList[i] = NULL;
	}
}

#include "game.h"

int main()
{	
	Game* pGame = Game::GetInstance();

	pGame->setup();
	pGame->loop();
	pGame->destroy();

	delete pGame;
	pGame = NULL;

	return 0;
}

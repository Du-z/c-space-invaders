#ifndef H_KEYBOARD
#define H_KEYBOARD

#include "actor.h"

class Keyboard : public ABActor
{
public:
	Keyboard();
	virtual ~Keyboard();

	virtual void setup(b2World* pB2World);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:
};

#endif // H_KEYBOARD
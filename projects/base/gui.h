#ifndef H_GUI
#define H_GUI

#include "actor.h"

class Gui : public ABActor
{
public:
	Gui();
	virtual ~Gui();

	virtual void setup(b2World* pB2World);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:
	void updString();

	sf::RectangleShape* uiBg;
	sf::Text* uiText;

	const std::string lvlStr, scoreStr, livesStr;
	int score;
};
#endif // H_GUI
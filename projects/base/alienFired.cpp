#include "alienFired.h"

AlienFired::AlienFired(const b2Vec2& pos)
{
	mEventType = Event::ALIEN_FIRED;
	mActedUpon = false;

	mPos = pos;
}

AlienFired::~AlienFired()
{

}
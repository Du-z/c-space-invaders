#include "shield.h"

#include "general.h"

int Shield::shieldsCreated = 0;

Shield::Shield()
{
	mActorType = Actor::SHIELD;
}

Shield::~Shield()
{

}

void Shield::setup(b2World* pB2World)
{
	float offset = 10;
	
	b2Vec2 pos(offset + ((screenWidthM - offset) / shieldCount) * (shieldsCreated % shieldCount), screenHeightM - 4.f);

	int count = 40;
	shieldPartList.resize(count);

	for(size_t i = 0; i < count; ++i)
	{
		shieldPartList[i] = new ShieldPart();
		shieldPartList[i]->setup(pB2World);

		int iPos = i;

		if(iPos >= 28)
			iPos += 4;
		if(iPos >= 40)
			iPos += 4;

		b2Vec2 partPos(pos.x + ShieldPart::size.x * (iPos % 12),pos.y + ShieldPart::size.y * (iPos / 12));
		shieldPartList[i]->setPos(partPos);
	}

	shieldsCreated++;
}

void Shield::simulation(sf::RenderWindow& window, float deltaT)
{
	for(size_t i = 0, size = shieldPartList.size(); i < size; ++i)
	{
	shieldPartList[i]->simulation(window, deltaT);
	}
}

void Shield::eventReceiver(ABEvent* theEvent)
{

}

void Shield::render(sf::RenderWindow& window)
{
	for(size_t i = 0, size = shieldPartList.size(); i < size; ++i)
	{
		shieldPartList[i]->render(window);
	}
}

void Shield::dispose()
{
	for(size_t i = 0, size = shieldPartList.size(); i < size; ++i)
	{
		shieldPartList[i]->dispose();
	}
}

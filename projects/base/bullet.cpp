#include "bullet.h"

#include "eventMgr.h"
#include "keyPressed.h"
#include "playerFired.h"
#include "collision.h"

Bullet::Bullet()
{
	mActorType = Actor::BULLET;

	deactivate = false;
	mShotFrom = Actor::UNSET;
}

Bullet::~Bullet()
{

}

 void Bullet::setup(b2World* pB2World)
{
	/////////////// SETUP Box 2D /////////////////
	b2BodyDef bodyDef;
	bodyDef.position.Set(-64.f, -64.f);
	bodyDef.type = b2_dynamicBody;
	bodyDef.fixedRotation = true;
	pBody = pB2World->CreateBody(&bodyDef);

	b2Vec2 size(0.2f, 0.5f); // Set size here
	b2PolygonShape shape;
	shape.SetAsBox(size.x / 2, size.y / 2, b2Vec2(0, 0), 0.f);

	b2FixtureDef fixtureDef;
    fixtureDef.friction = 0.7f; // set the friction the obj provide when it rubs on another obj
	fixtureDef.restitution = 0.25f; // set the elasticity of the obj
    fixtureDef.shape = &shape;

	pBody->CreateFixture(&fixtureDef);
	pBody->SetGravityScale(0);
	pBody->SetUserData(this);
	fixtureDef.isSensor = true;
	//pBody->SetActive(false);
	//////////////////////////////////////////////

	/////////////// SETUP DRAWABLE ///////////////
	pDrawable = new Drawable();
	pDrawable->setup("data/sprites.png");
	pDrawable->setTextureRect(0, 0, 1, 1);
	pDrawable->setSize(size);
	pDrawable->setOrigin(sf::Vector2f(1.f / 2, 1.f / 2));
	//////////////////////////////////////////////

	/////////////// SUB TO EVENTS ////////////////
	EventMgr::GetInstance()->addSubscriber(Event::PLAYER_FIRED, this);
	EventMgr::GetInstance()->addSubscriber(Event::ALIEN_FIRED, this);
	//////////////////////////////////////////////
}

void Bullet::simulation(sf::RenderWindow& window, float deltaT)
{
	if(pBody->IsActive())
	{
		if(pBody->GetPosition().y < 0 || pBody->GetPosition().y > screenHeightM || deactivate)
		{
			pBody->SetTransform(b2Vec2(-64.f, -64.f), 0);
			pBody->SetActive(false);

			deactivate = false;
			mShotFrom = Actor::UNSET;
			return;
		}
	
		pDrawable->setPosRot(*pBody);
	}
}

void Bullet::eventReceiver(ABEvent* theEvent)
{
	
	// Check the event type
	if(theEvent->getEventType() == Event::PLAYER_FIRED)
	{
		if(!pBody->IsActive()) // only need to shoot non-active body
		{
 			PlayerFired* playerFire = static_cast<PlayerFired*>(theEvent);
			if(!playerFire->getActedUpon())
			{
				pBody->SetActive(true);
				pBody->SetTransform(playerFire->getPos(), 0);
				pBody->SetLinearVelocity(b2Vec2(0, -bulletSpeed));

				pDrawable->setColour(sf::Color::Green);

				playerFire->hasBeenActedUpon();
				mShotFrom = Actor::PLAYER;
			}
		}
	}
	else if(theEvent->getEventType() == Event::ALIEN_FIRED)
	{
		if(!pBody->IsActive()) // only need to shoot non-active body
		{
			PlayerFired* playerFire = static_cast<PlayerFired*>(theEvent);
			if(!playerFire->getActedUpon())
			{
				pBody->SetActive(true);
				pBody->SetTransform(playerFire->getPos(), 0);
				pBody->SetLinearVelocity(b2Vec2(0, bulletSpeed / 3));

				pDrawable->setColour(sf::Color::Red);

				playerFire->hasBeenActedUpon();
				mShotFrom = Actor::ALIEN;
			}
		}
	}
	else if(theEvent->getEventType() == Event::COLLISION)
	{
		Collision* collision = static_cast<Collision*>(theEvent);
		if(collision->getOtherBody()->getActorType() == Actor::ALIEN && mShotFrom == Actor::PLAYER)
		{
			deactivate = true;
		}
		else if(collision->getOtherBody()->getActorType() == Actor::PLAYER && mShotFrom == Actor::ALIEN)
		{
			deactivate = true;
		}
		else if(collision->getOtherBody()->getActorType() == Actor::SHIELD_PART)
		{
			deactivate = true;
		}
	}
}

void Bullet::render(sf::RenderWindow& window)
{
	if(pBody->IsActive())
		pDrawable->draw(window);
}

void Bullet::dispose()
{
	pDrawable->dispose();
	delete pDrawable;
	pDrawable = NULL;

	pBody->GetWorld()->DestroyBody(pBody);
	pBody = NULL;
}

#ifndef H_SHIELD
#define H_SHIELD

#include "actor.h"

#include "shieldPart.h"

class Shield : public ABActor
{
public:
	Shield();
	virtual ~Shield();

	virtual void setup(b2World* pB2World);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:
	static int shieldsCreated;
	std::vector<ShieldPart*> shieldPartList;
};

#endif // H_SHIELD
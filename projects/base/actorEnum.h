#ifndef H_ACTOR_ENUM
#define H_ACTOR_ENUM

namespace Actor
{
	// the game loop will sort the actor list into the same order that these enums are in.
	enum Type
	{
		UNSET = -1,
		KEYBOARD,
		BACKGROUND,
		DEBRIS,
		BULLET,
		SHIELD,
		SHIELD_PART,
		ALIEN,
		PLAYER,
		EXPLOSION,
		GUI,
		LABEL,
		COUNT
	};
}

#endif // H_ACTOR_ENUM
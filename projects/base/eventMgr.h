#ifndef H_EVENT_MANGER
#define H_EVENT_MANGER

#include <list>
#include <vector>

#include "actor.h"
#include "event.h"

class EventMgr
{
public:
	~EventMgr();
	static EventMgr* GetInstance();

	void notifySubscribers();
	void addSubscriber(Event::Types eventType, ABActor* actor);
	void clearSubscribers();

	void addEvent(ABEvent* theEvent);

private:
	EventMgr();
	static EventMgr* mInstance;

	std::vector<std::vector<ABActor*>> subscriberList;
	std::vector<ABEvent*> eventList;
};

#endif // H_EVENT_MANGER
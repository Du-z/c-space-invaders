#ifndef H_BULLET
#define H_BULLET

#include "actor.h"

class Bullet : public ABActor
{
public:
	Bullet();
	virtual ~Bullet();

	virtual void setup(b2World* pB2World);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

	Actor::Type getShotFrom(){return mShotFrom;}

private:

	bool deactivate;

	Actor::Type mShotFrom;

	static const int bulletSpeed = 40;
};

#endif // H_BULLET
#include "debris.h"

#include "eventMgr.h"
#include "alienHit.h"

Debris::Debris()
{
	mActorType = Actor::DEBRIS;
}

Debris::~Debris()
{

}

 void Debris::setup(b2World* pB2World)
{
	/////////////// SETUP Box 2D /////////////////
	b2BodyDef bodyDef;
	bodyDef.position.Set(-64.f, -64.f);
	bodyDef.type = b2_dynamicBody;
	bodyDef.fixedRotation = true;
	pBody = pB2World->CreateBody(&bodyDef);

	b2Vec2 size(0.25f, 0.25f); // Set size here
	b2PolygonShape shape;
	shape.SetAsBox(size.x / 2, size.y / 2, b2Vec2(0, 0), 0.f);

	b2FixtureDef fixtureDef;
    fixtureDef.friction = 0.7f; // set the friction the obj provide when it rubs on another obj
	fixtureDef.restitution = 0.25f; // set the elasticity of the obj
    fixtureDef.shape = &shape;
	fixtureDef.isSensor = true;

	pBody->CreateFixture(&fixtureDef);
	pBody->SetUserData(this);
	//pBody->SetActive(false);
	//////////////////////////////////////////////

	/////////////// SETUP DRAWABLE ///////////////
	pDrawable = new Drawable();
	pDrawable->setup("data/sprites.png");
	pDrawable->setTextureRect(0, 0, 1, 1);
	pDrawable->setSize(size);
	pDrawable->setOrigin(sf::Vector2f(1.f / 2, 1.f / 2));
	//////////////////////////////////////////////

	/////////////// SUB TO EVENTS ////////////////
	EventMgr::GetInstance()->addSubscriber(Event::ALIEN_HIT, this);
	//////////////////////////////////////////////
}

void Debris::simulation(sf::RenderWindow& window, float deltaT)
{
	if(pBody->IsActive())
	{
		if(pBody->GetPosition().y < 0 || pBody->GetPosition().y > screenHeightM || deactivate)
		{
			pBody->SetTransform(b2Vec2(-64.f, -64.f), 0);
			pBody->SetActive(false);

			deactivate = false;
			return;
		}
	
		pDrawable->setPosRot(*pBody);
	}
}

void Debris::eventReceiver(ABEvent* theEvent)
{
	
	// Check the event type
	if(theEvent->getEventType() == Event::ALIEN_HIT)
	{
		AlienHit* alienHit = static_cast<AlienHit*>(theEvent);

		int chance = 4;
		if(alienHit->getAlienType() == MOTHERSHIP)
			chance = 2;

		if(!pBody->IsActive() && randInt(0, chance) == 0)
		{
			pBody->SetActive(true);
			pBody->SetTransform(alienHit->getPos(), 0);
			pBody->SetAngularVelocity(randFloat(-10, 10));
			pBody->SetLinearVelocity(b2Vec2(randFloat(-5, 5), randFloat(-10, 1)));

			if(alienHit->getAlienType() == MOTHERSHIP)
				pDrawable->setColour(sf::Color::Red);
			else
				pDrawable->setColour(sf::Color::White);
		}
	}
}

void Debris::render(sf::RenderWindow& window)
{
	if(pBody->IsActive()) // only needs to draw the bullet if it is active
		pDrawable->draw(window);
}

void Debris::dispose()
{
	pDrawable->dispose();
	delete pDrawable;
	pDrawable = NULL;

	pBody->GetWorld()->DestroyBody(pBody);
	pBody = NULL;
}

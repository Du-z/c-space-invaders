#ifndef H_ALIEN_FIRED
#define H_ALIEN_FIRED

#include "event.h"

#include "Box2D\Common\b2Math.h"

class AlienFired : public ABEvent
{
public:
	explicit AlienFired(const b2Vec2& pos);
	virtual ~AlienFired();

	bool getActedUpon(){return mActedUpon;}
	void hasBeenActedUpon(){mActedUpon = true;}

	b2Vec2& getPos(){return mPos;}

private:

	bool mActedUpon;
	b2Vec2 mPos;
};

#endif // H_ALIEN_FIRED
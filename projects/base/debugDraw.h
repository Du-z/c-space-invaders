// http://en.sfml-dev.org/forums/index.php?topic=9071.msg61192#msg61192

#ifndef H_DEBUG_DRAW
#define H_DEBUG_DRAW

#include <SFML\Graphics.hpp>
#include <Box2D\Box2D.h>

class DebugDraw : public b2Draw
{
public:
    DebugDraw();
    virtual ~DebugDraw(){};

    void LinkTarget(sf::RenderTarget& gtarget);
    virtual void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);
    virtual void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);
    virtual void DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color);
    virtual void DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color);
    virtual void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color);
    virtual void DrawTransform(const b2Transform &xf){};

private:
    sf::RenderTarget* m_target;
    //inliners for colour and point conversions
    inline sf::Color EEColor(const b2Color& gCol);
    inline sf::Vector2f EEVector(const b2Vec2& gVec){return sf::Vector2f(gVec.x*pixmeters,gVec.y*pixmeters);}
    const float pixmeters,radegrees;//constants for point and degree conversions
};

#endif // H_DEBUG_DRAW
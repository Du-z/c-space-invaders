#include "playerFired.h"

PlayerFired::PlayerFired(const b2Vec2& pos)
{
	mEventType = Event::PLAYER_FIRED;
	mActedUpon = false;
	
	mPos = pos;
}

PlayerFired::~PlayerFired()
{

}
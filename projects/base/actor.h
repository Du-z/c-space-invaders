#ifndef H_ACTOR
#define H_ACTOR

#include "SFML\Graphics\RenderWindow.hpp"
#include "drawable.h"
#include "Box2D\Box2D.h"

#include "actorEnum.h"
#include "event.h"

class ABActor
{
public:
	ABActor();
	virtual ~ABActor();

	Actor::Type getActorType(){return mActorType;}

	virtual void setup(b2World* pB2World) = 0;
	virtual void simulation(sf::RenderWindow& window, float deltaT) = 0;
	virtual void eventReceiver(ABEvent* theEvent) = 0;
	virtual void render(sf::RenderWindow& window) = 0;
	virtual void dispose() = 0;

protected:
	Drawable* pDrawable;
	b2Body* pBody;
	Actor::Type mActorType;

private:
};

#endif // H_ACTOR
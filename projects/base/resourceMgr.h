#ifndef H_RESOURCE_MANGER
#define H_RESOURCE_MANGER

#include <string>
#include <map>
#include <vector>

#include "SFML\Graphics.hpp"
#include "SFML\Audio\SoundBuffer.hpp"

class ResourceMgr
{
public:
	~ResourceMgr();
	static ResourceMgr* GetInstance();

	// returns the pointer to the data if it exist, else uses load() it.
	// If the file does not exist, creates a 1x1 magenta pixel
	sf::Texture* getTexture(const std::string& dir);
	bool destroyTexture(const std::string& dir); // removes the resource from the map, returns false on fail
	void destroyAllTextures();

	sf::Font* getFont(const std::string& dir);
	bool destroyFont(const std::string& dir); // removes the resource from the map, returns false on fail
	void destroyAllFonts();

	sf::SoundBuffer* getSound(const std::string& dir);
	bool destroySound(const std::string& dir); // removes the resource from the map, returns false on fail
	void destroyAllSounds();

	void destroyAll(); // removes all the resources from the map.

	// Returns the result as bytes
	size_t getTextureSize();
	size_t getAssetsSize();

private:
	ResourceMgr();
	static ResourceMgr* mInstance;

	// returns pointer if that resource was successfully loaded, NULL otherwise the returned vector MUST be cleaned up by the caller, load() will not do it!
	std::vector<char>* load(const std::string& dir); 

	std::map<std::string, sf::Texture*> textureList;
	std::map<std::string, sf::Font*> fontList;
	std::map<std::string, sf::SoundBuffer*> soundList;
};

#endif // H_RESOURCE_MANGER
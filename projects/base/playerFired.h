#ifndef H_PLAYER_FIRED
#define H_PLAYER_FIRED

#include "event.h"

#include "Box2D\Common\b2Math.h"

class PlayerFired : public ABEvent
{
public:
	explicit PlayerFired(const b2Vec2& pos);
	virtual ~PlayerFired();

	bool getActedUpon(){return mActedUpon;}
	void hasBeenActedUpon(){mActedUpon = true;}

	b2Vec2& getPos(){return mPos;}

private:

	bool mActedUpon;
	b2Vec2 mPos;
};

#endif // H_PLAYER_FIRED
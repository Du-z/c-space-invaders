#include "keyPressed.h"

KeyPressed::KeyPressed(const sf::Keyboard::Key key)
{
	mKey = key;
	mEventType = Event::KEY_PRESSED;
}

KeyPressed::~KeyPressed()
{

}
#include "screen.h"

#include <sstream>
#include "kf/kf_log.h"

Screen::Screen()
{
	nextScreen = NO_CHANGE;
}

Screen::~Screen()
{

}

void Screen::orderActorList()
{
	// uses http://en.wikipedia.org/wiki/Selection_sort but we sort the largest, not the smallest.

	size_t size = actorList.size(), loopCount = 0, swaps = 0;

	while(size > 1) // keep sorting until we run out of elements to sort.
	{		
		size_t largest = 0;
		for(size_t i = 0; i < size; ++i) // find the largest.
		{
			if(actorList[i]->getActorType() > actorList[largest]->getActorType())
				largest = i;
			loopCount++;
		}

		size--; // reduce the size to search by 1 because we dont need to check the value at the back because it is the largest (also it can conveniently be used below)

		if(largest != size) // we only have to swap if it is not the last val already
		{
			std::swap(actorList[largest], actorList[size]);
			swaps++;
		}
	}

	std::stringstream ss;
	ss << "Finished sorting Actors...\n\t Number of Actors = " << actorList.size() << "\n\t Number of Checks = " << loopCount << "\n\t Number of Swaps = " << swaps;
	kf_log(ss.str());
}

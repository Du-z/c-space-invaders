#ifndef H_MAIN_MENU
#define H_MAIN_MENU

#include "screen.h"

class MainMenu : public Screen
{
public:
	MainMenu();
	virtual ~MainMenu();

	virtual void setup(sf::RenderWindow& window);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

	static ScreenTypes nextScreenSelected;

private:
	sf::Text titleText, instructText;
};

#endif // H_MAIN_MENU
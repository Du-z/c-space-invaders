#ifndef H_GAME
#define H_GAME

#include "SFML/Graphics.hpp"
#include "screen.h"

class Game
{
public:
	~Game();
	static Game* GetInstance();

	void setup();
	void loop();
	void destroy();

private:
	Game();
	static Game* mInstance;

	sf::RenderWindow* window;
	Screen* theScreen;
};

#endif // H_GAME
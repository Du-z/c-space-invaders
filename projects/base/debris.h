#ifndef H_DEBRIS
#define H_DEBRIS

#include "actor.h"

class Debris : public ABActor
{
public:
	Debris();
	virtual ~Debris();

	virtual void setup(b2World* pB2World);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:
	bool deactivate;

	static const int bulletSpeed = 40;
};

#endif // H_DEBRIS
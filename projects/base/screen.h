#ifndef H_SCREEN
#define H_SCREEN

#include "SFML\Graphics\RenderWindow.hpp"

#include "actor.h"

enum ScreenTypes{NO_CHANGE = 0, MAIN_MENU, GAME_LOOP, CREDITS, EXIT};

class Screen
{
public:
	Screen();
	virtual ~Screen();

	virtual void setup(sf::RenderWindow& window) = 0;
	virtual void simulation(sf::RenderWindow& window, float deltaT) = 0;
	virtual void render(sf::RenderWindow& window) = 0;
	virtual void dispose() = 0;

	ScreenTypes screenChange(){return nextScreen;}
	void orderActorList();

protected:

	std::vector<ABActor*> actorList;

	ScreenTypes nextScreen;

private:
};

#endif // H_SCREEN
#include "game.h"

#include "kf/kf_log.h"
#include "qgf2d/system.h"

#include "general.h"
#include "resourceMgr.h"
#include "eventMgr.h"
#include "debugDraw.h"

#include "mainMenu.h"
#include "gameLoop.h"
#include "credits.h"


Game* Game::mInstance = NULL;

Game* Game::GetInstance()
{
	if (mInstance == NULL)
		mInstance = new Game();

	return mInstance;
}

Game::Game()
{
	window = NULL;
	theScreen = NULL;
}

Game::~Game()
{
	window = NULL;
	theScreen = NULL;

    mInstance = NULL;
}

void Game::setup()
{
	qgf::initDirectory();
	kf::Log::getDefault().addCout();
	kf::Log::getDefault().addFile("base.log");
	kf_log("Game Start");

	window = new sf::RenderWindow(sf::VideoMode(screenWidth, screenHeight, 32), "Game Engine");
	window->setFramerateLimit(60);

	ResourceMgr::GetInstance();

	EventMgr::GetInstance();

	theScreen = new MainMenu();
	theScreen->setup(*window);

	kf_log("Game Init Finished");
}

void Game::loop()
{
	sf::Clock deltaClock;
	float deltaT;

	while (window->isOpen())
	{
		deltaT = deltaClock.restart().asSeconds();
		
		EventMgr::GetInstance()->notifySubscribers();
		theScreen->simulation(*window, deltaT);
		theScreen->render(*window);

		//Next screen check
		ScreenTypes nextScreen = theScreen->screenChange();
		if(nextScreen != NO_CHANGE)
		{
			kf_log("Switching Screens");
			theScreen->dispose();
			EventMgr::GetInstance()->clearSubscribers();
			delete theScreen;

			switch (nextScreen)
			{
			case MAIN_MENU:
				theScreen = new MainMenu();
				break;
			case GAME_LOOP:
				theScreen = new GameLoop();
				break;
			case CREDITS:
				theScreen = new Credits();
				break;
			case EXIT:
				window->close();
				theScreen = NULL;
				break;
			default:
				window->close();
				theScreen = NULL;
				break;
			}

			if(theScreen != NULL)
			{
				theScreen->setup(*window);
				// sort the list based on enum order
				theScreen->orderActorList();
			}
		}
	}
}

void Game::destroy()
{
	delete window;
	window = NULL;

	delete theScreen;
	theScreen = NULL;

	ResourceMgr::GetInstance()->destroyAll();
}

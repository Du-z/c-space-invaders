#ifndef H_LABEL
#define H_LABEL

#include "actor.h"

class MenuLabel : public ABActor
{
public:
	MenuLabel();
	virtual ~MenuLabel();

	virtual void setup(b2World* pB2World);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

	void setString(const std::string& str){mText->setString(str);}

private:
	void updString();

	const static int COUNT = 3;

	std::string strList[COUNT];
	sf::Text* mText;

	float labelTimer;
	int curLabel;
};

#endif // H_LABEL
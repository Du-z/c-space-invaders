
#ifndef H_GENERAL
#define H_GENERAL

#include <stdlib.h>
#include <math.h>

static const int screenWidth = 800;
static const int screenHeight = 600;

static const char worldScale = 16;

static const float screenWidthM = (float) screenWidth / worldScale;
static const float screenHeightM = (float) screenHeight / worldScale - 3;

static const int bulletCount = 8;
static const int explosionCount = 2;
static const int shieldCount = 3;
static const int debrisCount = 64;

static const int mshipAlienCount = 1;
static const int tier1AlienCount = 20;
static const int tier2AlienCount = 20;
static const int tier3AlienCount = 20;

static float randFloat(float min, float max)
{
	float r = (float)rand() / (float)RAND_MAX;
	float retVal = min + r * (max - min);
	return retVal;
}

static int randInt(int min, int max)
{
	int retVal = rand() % (1 + max + abs(min)) - min;
	return retVal;
}

#endif // H_GENERAL
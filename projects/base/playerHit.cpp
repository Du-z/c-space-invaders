#include "playerHit.h"

#include "general.h"

PlayerHit::PlayerHit(const b2Vec2& pos)
{
	mEventType = Event::PLAYER_HIT;
	mActedUpon = false;

	mPos = pos;
}

PlayerHit::~PlayerHit()
{

}
#include "shieldPart.h"

#include "collision.h"
#include "eventMgr.h"

const b2Vec2 ShieldPart::size(0.35f,0.4f);

ShieldPart::ShieldPart()
{
	mActorType = Actor::SHIELD_PART;

	kill = false;
}

ShieldPart::~ShieldPart()
{

}

void ShieldPart::setup(b2World* pB2World)
{
	/////////////// SETUP Box 2D /////////////////
	b2BodyDef bodyDef;
	bodyDef.position.Set(-64.f, -64.f);
	bodyDef.type = b2_staticBody;
	bodyDef.fixedRotation = true;
	pBody = pB2World->CreateBody(&bodyDef);

	b2PolygonShape shape;
	shape.SetAsBox(size.x / 2, size.y / 2, b2Vec2(0, 0), 0.f);

	b2FixtureDef fixtureDef;
	fixtureDef.friction = 0.7f; // set the friction the obj provide when it rubs on another obj
	fixtureDef.restitution = 0.25f; // set the elasticity of the obj
	fixtureDef.shape = &shape;

	pBody->CreateFixture(&fixtureDef);
	pBody->SetUserData(this);
	//pBody->SetActive(false);
	//////////////////////////////////////////////

	/////////////// SETUP DRAWABLE ///////////////
	pDrawable = new Drawable();
	pDrawable->setup("data/sprites.png");
	pDrawable->setTextureRect(0, 0, 1, 1);
	pDrawable->setSize(size);
	pDrawable->setOrigin(sf::Vector2f(1.f / 2, 1.f / 2));
	pDrawable->setColour(sf::Color::Green);
	//////////////////////////////////////////////

	/////////////// SUB TO EVENTS ////////////////
	
	//////////////////////////////////////////////
}

void ShieldPart::simulation(sf::RenderWindow& window, float deltaT)
{
	if(pBody->IsActive())
	{
		pDrawable->setPosRot(*pBody);

		if(kill)
		{
			pBody->SetActive(false);
		}
	}
}

void ShieldPart::eventReceiver(ABEvent* theEvent)
{
	if(theEvent->getEventType() == Event::COLLISION)
	{
		Collision* collision = static_cast<Collision*>(theEvent);
		if(collision->getOtherBody()->getActorType() == Actor::BULLET)
		{
			kill = true;
		}
	}
}

void ShieldPart::render(sf::RenderWindow& window)
{
	if(pBody->IsActive())
		pDrawable->draw(window);
}

void ShieldPart::dispose()
{
	pDrawable->dispose();
	delete pDrawable;
	pDrawable = NULL;

	pBody->GetWorld()->DestroyBody(pBody);
	pBody = NULL;
}

void ShieldPart::setPos(b2Vec2 pos)
{
	pBody->SetTransform (pos, 0);
}


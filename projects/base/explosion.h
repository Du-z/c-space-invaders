#ifndef H_EXPLOSION
#define H_EXPLOSION

#include "actor.h"

class Explosion : public ABActor
{
public:
	Explosion();
	virtual ~Explosion();

	virtual void setup(b2World* pB2World);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

	void setPos(const b2Vec2& pos){pDrawable->setPos(sf::Vector2f(pos.x * worldScale, pos.y * worldScale));}

private:
	float life;
};

#endif // H_EXPLOSION
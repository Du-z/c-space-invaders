#ifndef H_ALIEN_HIT
#define H_ALIEN_HIT

#include "event.h"
#include "alien.h"

#include "Box2D\Common\b2Math.h"
#include "SFML\System\Vector2.hpp"

class AlienHit : public ABEvent
{
public:
	AlienHit(const b2Vec2& pos, AlienType alienType);
	virtual ~AlienHit();

	bool getActedUpon(){return mActedUpon;}
	void hasBeenActedUpon(){mActedUpon = true;}

	b2Vec2& getPos(){return mPos;}
	AlienType getAlienType(){return mAlienType;}

private:

	bool mActedUpon;
	b2Vec2 mPos;

	AlienType mAlienType;
};

#endif // H_ALIEN_HIT
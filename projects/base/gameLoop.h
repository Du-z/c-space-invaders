#ifndef H_GAME_LOOP
#define H_GAME_LOOP

#include "screen.h"
#include "collisionMgr.h"

class GameLoop : public Screen
{
public:
	GameLoop();
	virtual ~GameLoop();

	virtual void setup(sf::RenderWindow& window);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

	void nextLevel();
	static int getLevel(){return level;}

	void endGame();

private:

	CollisionMgr* collisionMgr;
	
	b2World* pB2World;

	static int level;
};

#endif // H_GAME_LOOP
#ifndef H_ALIEN
#define H_ALIEN

#include "actor.h"

#include "SFML/Audio/Sound.hpp"

enum AlienType {ALIEN_UNSET = -1, TIER_1, TIER_2, TIER_3, MOTHERSHIP};

enum MoveDirection {M_RIGHT, M_LEFT, M_DOWN};

class Alien : public ABActor
{
public:
	explicit Alien(AlienType alienType);
	virtual ~Alien();

	virtual void setup(b2World* pB2World);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

	void reset();

	static MoveDirection mMoveDirection;
	static MoveDirection mNextFrameMoveDirection;
	static float nextMinHeight;
	static int mAliensAlive;

private:

	void alienControls();
	void alienShoot();
	
	AlienType mAlienType;
	bool kill;

	b2Vec2 startPos;

	static float rowHeight, mshipTimer;

	sf::Sound* mShipFly;
	sf::Sound* alienHit;
};

#endif // H_ALIEN
#include "menuLabel.h"

#include "resourceMgr.h"

#include "eventMgr.h"
#include "keyPressed.h"

#include "screen.h"
#include "mainMenu.h"

MenuLabel::MenuLabel()
{
	mActorType = Actor::LABEL;

}

MenuLabel::~MenuLabel()
{

}

void MenuLabel::setup(b2World* pB2World)
{
	strList[0] = "Play Game";
	strList[1] = "Credits";
	strList[2] = "Exit Game";

	curLabel = 0;

	sf::Font* font = ResourceMgr::GetInstance()->getFont("data/victor-pixel.ttf");
	mText = new sf::Text("", *font, 50);
	mText->setColor(sf::Color::White);
	mText->setPosition(screenWidth / 2, screenHeight / 2);

	updString();

	/////////////// SUB TO EVENTS ////////////////
	EventMgr::GetInstance()->addSubscriber(Event::KEY_PRESSED, this);
	//////////////////////////////////////////////

	labelTimer = 0.25f;
}

void MenuLabel::simulation(sf::RenderWindow& window, float deltaT)
{
	labelTimer -= deltaT;
}

void MenuLabel::eventReceiver(ABEvent* theEvent)
{
	if(theEvent->getEventType() == Event::KEY_PRESSED)
	{
		if(labelTimer <= 0)
		{
			KeyPressed* key = static_cast<KeyPressed*>(theEvent);

			// LEFT/RIGHT
			if(key->getKey() == sf::Keyboard::Right)
			{
				curLabel++;
				curLabel = curLabel % COUNT;

				labelTimer = 0.25f;

				updString();
			}
			else if(key->getKey() == sf::Keyboard::Left)
			{
				curLabel--;
				if(curLabel < 0)
					curLabel = COUNT - 1;

				labelTimer = 0.25f;

				updString();
			}
			else if(key->getKey() == sf::Keyboard::Space)
			{
				if(curLabel == 0)
					MainMenu::nextScreenSelected = GAME_LOOP;
				else if(curLabel == 1)
					MainMenu::nextScreenSelected = CREDITS;
				else if(curLabel == 2)
					MainMenu::nextScreenSelected = EXIT;
			}
		}
	}
}

void MenuLabel::updString()
{
	mText->setString(strList[curLabel]);

	float oX = mText->getLocalBounds().width / 2;
	mText->setOrigin(oX, 25);
}

void MenuLabel::render(sf::RenderWindow& window)
{
	window.draw(*mText);
}

void MenuLabel::dispose()
{

}


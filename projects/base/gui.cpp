#include "gui.h"

#include <sstream>

#include "eventMgr.h"
#include "eventEnum.h"
#include "alienHit.h"
#include "resourceMgr.h"
#include "player.h"
#include "gameLoop.h"

Gui::Gui() : lvlStr("Level: "), scoreStr("Score: "), livesStr("Lives: ")
{
	mActorType = Actor::GUI;

	score = 0;
}

Gui::~Gui()
{
}

void Gui::setup(b2World* pB2World)
{
	uiBg = new sf::RectangleShape();
	uiBg->setSize(sf::Vector2f(screenWidth, 3 * worldScale));
	uiBg->setPosition(0,screenHeight - 3 * worldScale);
	uiBg->setFillColor(sf::Color(100, 100, 100, 200));

	sf::Font* font = ResourceMgr::GetInstance()->getFont("data/victor-pixel.ttf");
	uiText = new sf::Text("", *font, 35);
	uiText->setColor(sf::Color::White);
	uiText->setPosition(25, screenHeight - 3.f * worldScale);

	updString();

	
	/////////////// SUB TO EVENTS ////////////////
	EventMgr::GetInstance()->addSubscriber(Event::ALIEN_HIT, this);
	EventMgr::GetInstance()->addSubscriber(Event::PLAYER_HIT, this);
	//////////////////////////////////////////////
}

void Gui::simulation(sf::RenderWindow& window, float deltaT)
{

}

void Gui::eventReceiver(ABEvent* theEvent)
{
	if(theEvent->getEventType() == Event::ALIEN_HIT)
	{
		AlienHit* alienHit = static_cast<AlienHit*>(theEvent);

   		score += (alienHit->getAlienType() + 1) * 100;

		updString();
	}
	else if(theEvent->getEventType() == Event::PLAYER_HIT)
	{
		updString();
	}		
}

void Gui::updString()
{
	std::stringstream ss;
	ss << lvlStr << GameLoop::getLevel() << '\t' << livesStr << Player::getPlayerLives() << '\t' << scoreStr << score;
	uiText->setString(ss.str());
}

void Gui::render(sf::RenderWindow& window)
{
		window.draw(*uiBg);
		window.draw(*uiText);
}

void Gui::dispose()
{
	delete uiBg;
	delete uiText;
}
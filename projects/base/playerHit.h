#ifndef H_PLAYER_HIT
#define H_PLAYER_HIT

#include "event.h"
#include "player.h"

#include "Box2D\Common\b2Math.h"
#include "SFML\System\Vector2.hpp"

class PlayerHit : public ABEvent
{
public:
	explicit PlayerHit(const b2Vec2& pos);
	virtual ~PlayerHit();

	bool getActedUpon(){return mActedUpon;}
	void hasBeenActedUpon(){mActedUpon = true;}

	b2Vec2& getPos(){return mPos;}

private:

	bool mActedUpon;
	b2Vec2 mPos;
};

#endif // H_PLAYER_HIT
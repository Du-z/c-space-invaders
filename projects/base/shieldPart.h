#ifndef H_SHIELD_PART
#define H_SHIELD_PART

#include "actor.h"

class ShieldPart : public ABActor
{
public:
	ShieldPart();
	virtual ~ShieldPart();

	virtual void setup(b2World* pB2World);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

	void setPos(b2Vec2 pos);
	static const b2Vec2 size;

private:

	bool kill;
};

#endif // H_SHIELD_PART
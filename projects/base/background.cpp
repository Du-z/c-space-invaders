#include "background.h"

Background::Background()
{
	mActorType = Actor::BACKGROUND;
}

Background::~Background()
{

}

 void Background::setup(b2World* pB2World)
{
	/////////////// SETUP DRAWABLE ///////////////
	pDrawable = new Drawable();
	pDrawable->setup("data/bg.jpg");
	pDrawable->setScale(2);
	//////////////////////////////////////////////
}

void Background::simulation(sf::RenderWindow& window, float deltaT)
{
	
}

void Background::eventReceiver(ABEvent* theEvent)
{

}

void Background::render(sf::RenderWindow& window)
{
	pDrawable->draw(window);
}

void Background::dispose()
{
	pDrawable->dispose();
	delete pDrawable;
	pDrawable = NULL;
}

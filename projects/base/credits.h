#ifndef H_CREDITS
#define H_CREDITS

#include "screen.h"

class Credits : public Screen
{
public:
	Credits();
	virtual ~Credits();

	virtual void setup(sf::RenderWindow& window);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:
	sf::Text titleText, creditText;
};

#endif // H_CREDITS
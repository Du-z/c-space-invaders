#ifndef H_BACKGROUND
#define H_BACKGROUND

#include "actor.h"

class Background : public ABActor
{
public:
	Background();
	virtual ~Background();

	virtual void setup(b2World* pB2World);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

private:
};

#endif // H_BACKGROUND
#include "drawable.h"

#include "resourceMgr.h"
#include "qgf2d/anim.h"

Drawable::Drawable()
{
	mTexture = NULL;
	mSprite = NULL;
	mAnim = NULL;
}

Drawable::~Drawable()
{

}

void Drawable::setup(const std::string& textureDir)
{
	mTexture = ResourceMgr::GetInstance()->getTexture(textureDir);

	mSprite = new sf::Sprite();
	mSprite->setTexture(*mTexture);
}

void Drawable::setup(const std::string& textureDir,const b2Vec2& size)
{
	mTexture = ResourceMgr::GetInstance()->getTexture(textureDir);

	mSprite = new sf::Sprite();
	mSprite->setTexture(*mTexture);

	setSize(size);
}

void Drawable::setPosRot(const sf::Vector2f& pos, float rot)
{
	if(mSprite != NULL)
	{
		mSprite->setPosition(pos);
		mSprite->setRotation(rot);
	}
}

void Drawable::setFlipX()
{
	if(isFliped)
	{
		mSprite->setScale(fabs(mSprite->getScale().x), mSprite->getScale().y);
		isFliped = false;
	}
	else
	{
		mSprite->setScale(-mSprite->getScale().x, mSprite->getScale().y);
		isFliped = true;
	}
}

void Drawable::addAnim(int startX, int startY, int frameWidth, int frameHeight, int columns, int rows)
{
	if(mAnim != NULL)
	{
		delete mAnim;
		mAnim = NULL;
	}
	
	if(mSprite != NULL)
		mAnim = new qgf::Anim(mSprite, startX, startY, frameWidth, frameHeight, columns, rows);
}

void Drawable::setSize(const b2Vec2& size)
{
	setSize(size.x * worldScale, size.y * worldScale);
}

void Drawable::setSize(const sf::Vector2f& size)
{
	setSize(size.x, size.y);
}

void Drawable::setSize(float x, float y)
{
	float scaleX = (1.0f / mSprite->getTextureRect().width) * x;
	float scaleY = (1.0f / mSprite->getTextureRect().height) * y;
	mSprite->setScale(scaleX, scaleY);
}

void Drawable::setTextureRect(int left, int top, int width, int height)
{
	mSprite->setTextureRect(sf::IntRect(left, top, width, height));
}

void Drawable::dispose()
{
	if(mTexture != NULL)
	{
		// The texture should not be deleted, the AssetManager takes care of that.
		mTexture = NULL;
	}

	if(mSprite != NULL)
	{
		delete mSprite;
		mSprite = NULL;
	}

	if(mAnim != NULL)
	{
		delete mAnim;
		mAnim = NULL;
	}
}



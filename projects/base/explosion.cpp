#include "explosion.h"

#include "eventMgr.h"
#include "alienHit.h"
#include "playerHit.h"

Explosion::Explosion()
{
	mActorType = Actor::EXPLOSION;

	life = 0;
}

Explosion::~Explosion()
{

}

 void Explosion::setup(b2World* pB2World)
{
	b2Vec2 size(2, 2);
	
	/////////////// SETUP DRAWABLE ///////////////
	pDrawable = new Drawable();
	pDrawable->setup("data/sprites.png");
	pDrawable->setTextureRect(48, 0, 26, 16);
	pDrawable->setSize(size);
	pDrawable->setPos(sf::Vector2f(-64.f * worldScale, -64.f * worldScale));
	pDrawable->setOrigin(sf::Vector2f(16.f / 2, 16.f / 2));
	//////////////////////////////////////////////

	/////////////// SUB TO EVENTS ////////////////
	EventMgr::GetInstance()->addSubscriber(Event::ALIEN_HIT, this);
	EventMgr::GetInstance()->addSubscriber(Event::PLAYER_HIT, this);
	//////////////////////////////////////////////
}

void Explosion::simulation(sf::RenderWindow& window, float deltaT)
{
	if(life > 0)
	{
		life -= deltaT;
	}
	else
	{
		pDrawable->setPos(sf::Vector2f(-64.f * worldScale, -64.f * worldScale));
	}
}

void Explosion::eventReceiver(ABEvent* theEvent)
{
	if(theEvent->getEventType() == Event::ALIEN_HIT)
	{
		if(life <= 0)
		{
			AlienHit* alienHit = static_cast<AlienHit*>(theEvent);
			if(!alienHit->getActedUpon())
			{
				pDrawable->setPos(sf::Vector2f(alienHit->getPos().x * worldScale, alienHit->getPos().y * worldScale));

				life = 0.3f;
				alienHit->hasBeenActedUpon();
			}
		}
	}
	if(theEvent->getEventType() == Event::PLAYER_HIT)
	{
		if(life <= 0)
		{
			PlayerHit* playerHit = static_cast<PlayerHit*>(theEvent);
			if(!playerHit->getActedUpon())
			{
				pDrawable->setPos(sf::Vector2f((playerHit->getPos().x - 0.5f) * worldScale, playerHit->getPos().y * worldScale));

				life = 0.3f;
				playerHit->hasBeenActedUpon();
			}
		}
	}
}

void Explosion::render(sf::RenderWindow& window)
{
	if(life > 0)
		pDrawable->draw(window);
}

void Explosion::dispose()
{
	pDrawable->dispose();
	delete pDrawable;
	pDrawable = NULL;
}
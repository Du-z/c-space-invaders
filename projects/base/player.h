#ifndef H_PLAYER
#define H_PLAYER

#include "actor.h"
#include "SFML/Audio/Sound.hpp"

enum MoveType {NONE, RIGHT, LEFT};

class Player : public ABActor
{
public:
	Player();
	virtual ~Player();

	virtual void setup(b2World* pB2World);
	virtual void simulation(sf::RenderWindow& window, float deltaT);
	virtual void eventReceiver(ABEvent* theEvent);
	virtual void render(sf::RenderWindow& window);
	virtual void dispose();

	static int getPlayerLives(){return mPlayerLives;}

private:
	static int mPlayerLives;
	MoveType moveType;

	float fireCountdown;
	float roundPerSecond;

	sf::Sound* shoot;
	sf::Sound* playerHit;
};

#endif // H_PLAYER